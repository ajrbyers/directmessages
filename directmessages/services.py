from __future__ import unicode_literals

from .models import Message, BlockedUser
from .signals import message_read, message_sent
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db.models import Q
from django.http import Http404


class MessagingService(object):
    """
    A object to manage all messages and conversations
    """

    # Message creation

    def send_message(self, sender, recipient, message):
        """
        Send a new message
        :param sender: user
        :param recipient: user
        :param message: String
        :return: Message and status code
        """
        recipient_blocked_users = self.list_of_blocked_user_objects(recipient)

        if sender == recipient:
            raise ValidationError("You can't send messages to yourself.")

        if sender in recipient_blocked_users:
            raise ValidationError("You cannot send messages to this user.")

        message = Message(sender=sender, recipient=recipient, content=str(message))
        message.save()

        message_sent.send(sender=message, from_user=message.sender, to=message.recipient)

        # The second value acts as a status value
        return message, 200

    # Message reading
    def get_all_messages(self, user):
        """
        List of all user messages
        :param user: user account
        :return: messages
        """
        return Message.objects.filter(recipient=user)

    def get_unread_messages(self, user):
        """
        List of unread messages for a specific user
        :param user: user
        :return: messages
        """
        return Message.objects.all().filter(recipient=user, read_at=None)

    def unread_message_count(self, user):
        """
        Gets a count of unread messages
        :param user: user
        :return: message_count <int>
        """
        return self.get_unread_messages(user).count()

    def get_sent_messages(self, user):
        """
        List of sent messages from a user
        :param user: user
        :return: messages
        """
        return Message.objects.filter(sender=user).order_by('-sent_at')

    def read_message(self, message_id, user):
        """
        Read specific message
        :param message_id: Integer
        :return: Message Text
        """
        try:
            message = Message.objects.get(id=message_id, recipient=user)
            self.mark_as_read(message)
            return message
        except Message.DoesNotExist:
            raise Http404

    def read_message_formatted(self, message_id):
        """
        Read a message in the format <User>: <Message>
        :param message_id: Id
        :return: Formatted Message Text
        """
        try:
            message = Message.objects.get(id=message_id)
            self.mark_as_read(message)
            return message.sender.username + ": "+message.content
        except Message.DoesNotExist:
            return ""

    # Conversation management

    def get_conversations(self, user):
        """
        Lists all conversation-partners for a specific user
        :param user: User
        :return: Conversation list
        """
        all_conversations = Message.objects.all().filter(Q(sender=user) | Q(recipient=user))

        contacts = []
        for conversation in all_conversations:
            if conversation.sender != user:
                contacts.append(conversation.sender)
            elif conversation.recipient != user:
                contacts.append(conversation.recipient)

        # To abolish duplicates
        return list(set(contacts))

    def get_conversation(self, user1, user2, limit=None, reversed=False, mark_read=False):
        """
        List of messages between two users
        :param user1: User
        :param user2: User
        :param limit: int
        :param reversed: Boolean - Makes the newest message be at index 0
        :return: messages
        """
        users = [user1, user2]

        # Newest message first if it's reversed (index 0)
        if reversed:
            order = '-pk'
        else:
            order = 'pk'

        conversation = Message.objects.all().filter(sender__in=users, recipient__in=users).order_by(order)

        if limit:
            # Limit number of messages to the x newest
            conversation = conversation[:limit]

        if mark_read:
            for message in conversation:
                # Just to be sure, everything is read
                self.mark_as_read(message)

        return conversation

    # Helper methods
    def mark_as_read(self, message):
        """
        Marks a message as read, if it hasn't been read before
        :param message: Message
        """

        if message.read_at is None:
            message.read_at = timezone.now()
            message_read.send(sender=message, from_user=message.sender, to=message.recipient)
            message.save()

    def delete_message(self, message_id, user):
        """
        Deletes a given message.
        """
        try:
            message = Message.objects.filter(pk=message_id, recipient=user)
            message.delete()
            return True
        except Message.DoesNotExist:
            return False

    def blocked_user_list(self, user):
        """
        Returns a list of blocked users for a given user
        """
        return BlockedUser.objects.filter(blocked_by_id=user.pk)

    def list_of_blocked_user_objects(self, user):
        user_list = list()

        for blocked_user in BlockedUser.objects.filter(blocked_by=user):
            user_list.append(blocked_user.blocked_user)

        return user_list

    def unblock_user(self, user, block_id):
        """
        Deletes a blocked user record
        """
        try:
            blocked_user = BlockedUser.objects.get(blocked_by_id=user.pk, pk=block_id)
            blocked_user.delete()
            return 'Record deleted.'
        except BlockedUser.DoesNotExist:
            return 'Record not found.'

    def add_blocked_user(self, blocked_by, blocked_user):
        """
        Blocks a user
        """
        BlockedUser.objects.get_or_create(
            blocked_by=blocked_by,
            blocked_user=blocked_user
        )
