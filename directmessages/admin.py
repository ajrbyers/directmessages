from .models import Message, BlockedUser
from django.contrib import admin

class MessageAdmin(admin.ModelAdmin):
    model = Message
    list_display = ('id', 'sender', 'subject', 'sent_at', 'read_at')


class BlockedUserAdmin(admin.ModelAdmin):
	model = BlockedUser
	list_display = ('id', 'blocked_by', 'blocked_user', 'date_blocked')

admin.site.register(Message, MessageAdmin)
admin.site.register(BlockedUser, BlockedUserAdmin)