from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


@python_2_unicode_compatible
class Message(models.Model):
    """
    A private directmessage
    """
    content = models.TextField(_('Content'))
    subject = models.CharField(max_length=255, null=True)
    sender = models.ForeignKey(AUTH_USER_MODEL, related_name='sent_dm', verbose_name=_("Sender"), null=True, on_delete=models.SET_NULL)
    recipient = models.ForeignKey(AUTH_USER_MODEL, related_name='received_dm', verbose_name=_("Recipient"), null=True,  on_delete=models.SET_NULL)
    sent_at = models.DateTimeField(_("sent at"), null=True, blank=True)
    read_at = models.DateTimeField(_("read at"), null=True, blank=True)

    @property
    def unread(self):
        """returns whether the message was read or not"""
        if self.read_at is not None:
            return False
        return True

    @property
    def reply_to_subject(self):
        if not self.subject.startswith('Re'):
            return "Re: {subject}".format(subject=self.subject)
        else:
            return self.subject

    def __str__(self):
        return self.content

    def save(self, **kwargs):
        if self.sender == self.recipient:
            raise ValidationError("You can't send messages to yourself")

        if not self.id:
            self.sent_at = timezone.now()
        super(Message, self).save(**kwargs)


class BlockedUser(models.Model):
    """
    A blocked user
    """
    blocked_by = models.ForeignKey(AUTH_USER_MODEL, related_name="user_blocking", null=True, on_delete=models.SET_NULL)
    blocked_user = models.ForeignKey(AUTH_USER_MODEL, related_name="user_blocked", null=True, on_delete=models.SET_NULL)
    date_blocked = models.DateTimeField(_("blocked on"), null=True, blank=True)

    def save(self, **kwargs):
        if self.blocked_by == self.blocked_user:
            raise ValidationError("You can't block yourself")

        if not self.id:
            self.date_blocked = timezone.now()
        super(BlockedUser, self).save(**kwargs)

    def __str__(self):
        return "{0} blocked by {1}".format(self.blocked_user, self.blocked_by)